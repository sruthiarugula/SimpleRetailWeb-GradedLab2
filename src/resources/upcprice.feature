Feature: UPC and Price is valid on page
Ensure proper Price on are on shipment.jsp

@web
Scenario: UPC and Price match
	Given the following Data:
	|567321101987|19.99	|
	|567321101986|17.99	|
	|567321101985|20.49	|
	|567321101984|23.88 |
	|467321101899|9.75  |
	|477321101878|17.25 |
	
	When User Navigates to Create Shipment Page
	
	Then upc and price have to be matched