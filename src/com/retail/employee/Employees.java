package com.retail.employee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Employees {
	private List<EmployeeBean> empBean;

	public Employees() {

		empBean = new ArrayList<EmployeeBean>();

		empBean.add(new EmployeeBean(123, "Justin", "Poole", 40));
		empBean.add(new EmployeeBean(124, "Ed", "Lance", 40));
		empBean.add(new EmployeeBean(125, "Sruthi", "Arugula", 50));
		empBean.add(new EmployeeBean(126, "Tor", "Swennumenson", 20));
	}

	public List<EmployeeBean> getEmployees() {

		empBean = validateWorkingHours(empBean);
		return empBean;
	}

	List<EmployeeBean> validateWorkingHours(List<EmployeeBean> emp2Bean) {
		if (emp2Bean != null) {
			for (int i = 0; i < emp2Bean.size(); i++) {
				if (emp2Bean.get(i).getWorkingHours() < 40) {
					empBean.remove(i);
				}
			}

		}
		return empBean;
	}

	public Map<Integer, String> nameFormulation() {
		empBean = validateWorkingHours(empBean);
		String value;
		Map<Integer, String> map = new HashMap<Integer, String>();
		for (int i = 0; i < empBean.size(); i++) {
			value = this.empBean.get(i).getEmpFirstName() + " " + this.empBean.get(i).getEmpLastName();
			map.put(empBean.get(i).getEmployeeId(), value);
		}
		return map;
	}

}
