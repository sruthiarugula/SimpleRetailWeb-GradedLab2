package com.retail.employee;

public class EmployeeBean {

	private int employeeId;
	private String empFirstName;
	private String empLastName;
	private int workingHours;

	public int getEmployeeId() {
		return employeeId;
	}

	public EmployeeBean(int employeeId, String empFirstName, String empLastName, int workingHours)
			throws IllegalArgumentException {
		super();
		this.employeeId = employeeId;
		if (this.employeeId < 0) {
			throw new IllegalArgumentException("Employee Id cannot be negative");
		}
		this.empFirstName = empFirstName;
		this.empLastName = empLastName;
		if (workingHours < 0) {
			throw new IllegalArgumentException("Working Hours cannot be negative");
		}
		this.workingHours = workingHours;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmpFirstName() {
		return empFirstName;
	}

	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}

	public String getEmpLastName() {
		return empLastName;
	}

	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}

	public int getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(int workingHours) {
		if (workingHours < 0) {
			throw new IllegalArgumentException("Working Hours cannot be negative");
		}
		this.workingHours = workingHours;
	}

}
