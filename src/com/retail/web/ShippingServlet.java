package com.retail.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.retail.core.Item;
import com.retail.core.Products;

import com.retail.employee.Employees;

/**
 * Servlet implementation class ShippingServlet
 */
@WebServlet("/ShippingServlet")
public class ShippingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Item> myItems = Products.getItems();
		Employees emps = new Employees();
		Map<Integer, String> employees = emps.nameFormulation();
		//List<String> employeeNames = new ArrayList<String>(employees.values());
		request.getSession().setAttribute("items", myItems);
		//request.getSession().setAttribute("employeeNames", employeeNames);
		request.getSession().setAttribute("mapNames", employees);

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/shipment.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String[] checkedIds = request.getParameterValues("checkedRows");
	//	int employeeId=Integer.parseInt(request.getParameter("selectedKey"));
		String employeeId=request.getParameter("selectedKey");
		
		
	//	System.out.println("employee Id is "+ employeeId);
		List<Item> shipItems = (List<Item>) request.getSession().getAttribute("items");

		for (Iterator<Item> iter = (Iterator<Item>) shipItems.listIterator(); iter.hasNext();) {
			if (!(Arrays.asList(checkedIds).contains(iter.next().getUpc()))) {
				iter.remove();
			}

		}

		request.getSession().setAttribute("shipItems", shipItems);
		request.getSession().setAttribute("employeeId", employeeId);

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/shipConfirmation.jsp");
		dispatcher.forward(request, response);
	}

}
