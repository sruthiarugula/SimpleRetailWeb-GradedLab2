package com.retail.webd;

import static org.junit.Assert.*;

import java.util.ArrayList;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.retail.employee.EmployeeBean;
import com.retail.employee.Employees;

public class EmployeeTest {

	EmployeeBean e1;
	List<EmployeeBean> empBean;
	Employees emps;

	@Before
	public void setUp() throws Exception {
		empBean = new ArrayList<EmployeeBean>();
		emps = new Employees();
	}

	@Rule
	public ExpectedException negativeException = ExpectedException.none();

	@Test
	public void EmployeeWorkingHoursTest() {
		negativeException.expect(IllegalArgumentException.class);
		negativeException.expectMessage("Working Hours cannot be negative");
		e1 = new EmployeeBean(123, "Justin", "Poole", -40);

	}

	@Test
	public void EmployeeIdTest() {
		negativeException.expect(IllegalArgumentException.class);
		negativeException.expectMessage("Employee Id cannot be negative");
		e1 = new EmployeeBean(-123, "Justin", "Poole", 40);

	}

	@Test
	public void validateWorkingHours() {

		empBean = emps.getEmployees();
		// employees with valid working hours are three
		assertEquals(3, empBean.size(), 0.0001);
	}

}
