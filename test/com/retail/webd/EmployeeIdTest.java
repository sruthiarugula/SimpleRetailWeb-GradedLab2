package com.retail.webd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EmployeeIdTest {

	WebDriver driver;
	String name;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "D:/" + "/chromedriver.exe");
		driver = new ChromeDriver();

	}

	@Given("^User goes to shipment jsp Page and chooses Justin Poole$")
	public void user_goes_to_shipment_jsp_Page_and_chooses_Justin_Poole() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
		driver.get("http://localhost:8082/SimpleRetailWeb_starter/");
		WebElement element = driver.findElement(By.xpath("/html/body/form/select"));

		name = "Justin Poole";

		element.click();

		element = driver.findElement(By.xpath("/html/body/form/input"));
		element.submit();
		// WebElement
	}

	@When("^user clicks on createShipment button$")
	public void user_clicks_on_createShipment_button() throws Throwable {
		WebElement ele1 = driver.findElement(By.xpath("/html/body/form/input"));
		ele1.submit();

	}

	@Given("^User goes to shipment jsp Page and chooses Ed Lance$")
	public void user_goes_to_shipment_jsp_Page_and_chooses_Ed_Lance() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
		driver.get("http://localhost:8082/SimpleRetailWeb_starter/");
		WebElement element = driver.findElement(By.xpath("/html/body/form/select"));

		name = "Ed Lance";

		element.click();

		// WebElement
	}

	@Given("^User goes to shipment jsp Page and chooses Sruthi Arugula$")
	public void user_goes_to_shipment_jsp_Page_and_chooses_Sruthi_Arugula() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
		driver.get("http://localhost:8082/SimpleRetailWeb_starter/");
		WebElement element = driver.findElement(By.xpath("/html/body/form/select"));

		name = "Sruthi Arugula";

		element.click();
	}

	@Then("^employeeId should be (\\d+)$")
	public void employeeid_should_be(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
		if (name.equals("Justin Poole")) {

			assertEquals(123, arg1);
		} else if (name.equals("Ed Lance")) {
			assertEquals(124, arg1);
		} else {
			assertEquals(125, arg1);
		}
	}

}
