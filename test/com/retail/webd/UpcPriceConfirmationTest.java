package com.retail.webd;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UpcPriceConfirmationTest {

	WebDriver driver;
	Map<String, Double> upcExpectedPriceMap;
	Map<String, Double> upcActualPriceMap;
	List<String> upcList;
	List<String> priceList;
	List<WebElement> upcElement;
	List<WebElement> priceElement;

	@Before("@web")
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "D:/" + "/chromedriver.exe");
		driver = new ChromeDriver();
		upcExpectedPriceMap = new HashMap<String, Double>();
		upcActualPriceMap = new HashMap<String, Double>();
		upcList = new ArrayList<String>();
		priceList = new ArrayList<String>();
	}

	@Given("^the following Data:$")
	public void the_following_Data(Map<String, Double> upcPriceMap) throws Throwable {

		this.upcExpectedPriceMap = upcPriceMap;

	}

	@When("^User Navigates to Create Shipment Page$")
	public void user_Navigates_to_Create_Shipment_Page() throws Throwable {
		System.out.println("entered this method");
		// driver.g
		// driver.get("http://localhost:8080/SimpleRetailWeb_starter/");
		driver.get("http://localhost:8082/SimpleRetailWeb_starter/");
		System.out.println("hey successfully wen to there");
		WebElement element = driver.findElement(By.xpath("/html/body/form/input"));
		// System.out.println("foound out the webelement");
		element.click();

		upcElement = driver.findElements(By.xpath("/html/body/form/table/tbody/tr/td[2]"));

		priceElement = driver.findElements(By.xpath("/html/body/form/table/tbody/tr/td[4]"));

	}

	@Then("^upc and price have to be matched$")
	public void upc_and_price_have_to_be_matched() throws Throwable {

		for (int i = 0; i < upcElement.size(); i++) {

			// System.out.println("upc and price" + upc +" " + price);

			upcActualPriceMap.put((upcElement.get(i)).getText(), Double.parseDouble(priceElement.get(i).getText()));

		}

		// System.out.println("expected Map" + upcExpectedPriceMap);
		// System.out.println("Actual map" + upcActualPriceMap);
		assertTrue(upcExpectedPriceMap.equals(upcActualPriceMap));
	}

}
