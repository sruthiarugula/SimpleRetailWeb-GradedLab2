package com.retail.webd;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.apache.commons.io.FileUtils;
import org.junit.Before;




public class CaptureJenkins {

	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "D:/" + "/chromedriver.exe");
		driver = new ChromeDriver();

	}

	@Test

	public void JenkinsScreenShotter() {
		driver.get("http://localhost:8080");

		WebElement elemen = driver.findElement(By.id("j_username"));

		elemen.sendKeys("admin");
		elemen = driver.findElement(By.name("j_password"));
		elemen.sendKeys("Sdnrp@91");
		elemen=driver.findElement(By.id("yui-gen1-button"));
		elemen.click();
		elemen=driver.findElement(By.linkText("SimpleRetail_GradedLab2"));
		elemen.click();
		try {
			getscreenshot();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void getscreenshot() throws Exception 
    {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
         //The below method will save the screen shot in d drive with name "screenshot.png"
            FileUtils.copyFile(scrFile, new File("D:\\jenkins.png"));
    }


}
