package com.retail.webd;

import static org.junit.Assert.*;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GitScreenshotTest {

	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "D:/" + "/chromedriver.exe");
		driver = new ChromeDriver();

	}

	@Test

	public void JenkinsScreenShotter() {
		driver.get("https://gitlab.com/users/sign_in");

		WebElement elemen = driver.findElement(By.id("user_login"));

		elemen.sendKeys("sruthiarugula");
		elemen = driver.findElement(By.id("user_password"));
		elemen.sendKeys("Sdnrp@91");
		elemen=driver.findElement(By.name("commit"));
		elemen.click();
		elemen=driver.findElement(By.partialLinkText("SimpleRetailWeb-GradedLab2"));
		elemen.click();
		try {
			getscreenshot();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void getscreenshot() throws Exception 
    {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
         //The below method will save the screen shot in d drive with name "screenshot.png"
            FileUtils.copyFile(scrFile, new File("D:\\Git.png"));
    }



}
