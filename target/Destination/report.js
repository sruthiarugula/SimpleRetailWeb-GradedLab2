$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("upcprice.feature");
formatter.feature({
  "line": 1,
  "name": "UPC and Price is valid on page",
  "description": "Ensure proper Price on are on shipment.jsp",
  "id": "upc-and-price-is-valid-on-page",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4262419886,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "UPC and Price match",
  "description": "",
  "id": "upc-and-price-is-valid-on-page;upc-and-price-match",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@web"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "the following Data:",
  "rows": [
    {
      "cells": [
        "567321101987",
        "19.99"
      ],
      "line": 7
    },
    {
      "cells": [
        "567321101986",
        "17.99"
      ],
      "line": 8
    },
    {
      "cells": [
        "567321101985",
        "20.49"
      ],
      "line": 9
    },
    {
      "cells": [
        "567321101984",
        "23.88"
      ],
      "line": 10
    },
    {
      "cells": [
        "467321101899",
        "9.75"
      ],
      "line": 11
    },
    {
      "cells": [
        "477321101878",
        "17.25"
      ],
      "line": 12
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "User Navigates to Create Shipment Page",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "upc and price have to be matched",
  "keyword": "Then "
});
formatter.match({
  "location": "UpcPriceConfirmationTest.the_following_Data(String,Double\u003e)"
});
formatter.result({
  "duration": 119717757,
  "status": "passed"
});
formatter.match({
  "location": "UpcPriceConfirmationTest.user_Navigates_to_Create_Shipment_Page()"
});
formatter.result({
  "duration": 1091892887,
  "status": "passed"
});
formatter.match({
  "location": "UpcPriceConfirmationTest.upc_and_price_have_to_be_matched()"
});
formatter.result({
  "duration": 336233238,
  "status": "passed"
});
});